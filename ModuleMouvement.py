from rrb3 import *
import detecproxi
import Module_Audio


class mouvement(detecproxi.detection,Module_Audio.Audio):
    def __init__(self):
        self.val = ""
        self.proxdroite =0
        self.proxgauche = 0
        self.rr = RRB3(9.0, 6.0)
        self.MusiTemps = 1
        self.MusiTempsRotgauche = 1
        self.MusiTempsRotdroite = 1
        self.xx = 0
        self.yx = 0
        self.av = 0

    def avancer(self,Vx,x,Vy,y):
        self.val = self.obstacle()
        self.MusiTempsRotgauche = 1
        self.MusiTempsRotdroite = 1

        if  self.val == "droite":
            self.sons('PathAdjust', 1)
            self.proxdroite = 0
            self.xx = 1
            self.yx = 0
            self.av = 1
        elif  self.val == "gauche":
            self.sons('PathAdjust', 1)
            self.proxgauche = 0
            self.xx = 0
            self.yx = 1
            self.av = 1
        else:
            self.av = 0
            self.proxdroite = 0
            self.proxgauche = 0

        if self.av == 1 :
            self.rr.set_motors(Vx-self.proxdroite,self.xx,Vy-self.proxgauche,self.yx)
        elif self.av == 0 :
            self.rr.set_motors(Vx,x, Vy,y)

        if self.MusiTemps == 1:
            self.sons('StartEngine',1)
            self.MusiTemps = 0


    def reculer(self,seconde):
        self.rr.reverse(seconde,0.6)


    def rotation_gauche(self,seconde):
        if self.MusiTempsRotgauche == 1:
            self.sons('PathAdjust', 1)
            self.MusiTempsRot = 0

        self.rr.left(seconde,0.6)


    def rotation_droite(self,seconde):
        if self.MusiTempsRotdroite == 1:
            self.sons('PathAdjust', 1)
        self.rr.right(seconde,0.6)

    def dist(self):
        return self.rr.get_distance()

    def stop(self):
        self.rr.set_motors(0, 0, 0, 0)