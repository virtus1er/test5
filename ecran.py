import lcddriver
lcd = lcddriver.lcd()

class ecran:
    def __init__(self):
        self.lignes = "1"
        self.texte = " "

    def affichageHEUREUX(self):
        lcd.lcd_display_string("  @             @ ",1)
        lcd.lcd_display_string(" @   (o)   (o)   @", 2)
        lcd.lcd_display_string(" @       <       @", 3)
        lcd.lcd_display_string("  @      __/    @ ", 4)

    def affichageTRISTE(self):
        lcd.lcd_display_string("  @             @ ",1)
        lcd.lcd_display_string(" @   (o)   (o)   @", 2)
        lcd.lcd_display_string(" @    :  <  :    @", 3)
        lcd.lcd_display_string("  @     ___     @ ", 4)

    def affichage(self):
        lcd.lcd_display_string("  @             @ ",1)
        lcd.lcd_display_string(" @     >   <     @", 2)
        lcd.lcd_display_string(" @       <       @", 3)
        lcd.lcd_display_string("  @     ___     @ ", 4)