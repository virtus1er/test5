#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

from pygame import mixer
import time

mixer.init()

class Audio():

    def __init__(self):
        self.vol = 0.0
        self.read = ""

    def sons(self, piste, volume):  # Selecioner la piste et le volume
        self.vol = volume

        if piste == 'Found':
            self.read = mixer.music.load("sounds/foundobject2dance.wav")
        elif piste == 'StartEngine':
            self.read = mixer.music.load("sounds/wibigo.wav")
        elif piste == 'Picture':
            self.read = mixer.music.load("sounds/picture2.wav")
        elif piste == 'ObsWall':
            self.read = mixer.music.load("sounds/tuyauWall.wav")
        elif piste == 'Right':
            self.read = mixer.music.load("sounds/Droide1017.wav")
        elif piste == 'Left':
            self.read = mixer.music.load("sounds/Droide7182.wav")
        elif piste == 'Backwards':
            self.read = mixer.music.load("sounds/backward.wav")
        elif piste == 'Lowbattery':
            self.read = mixer.music.load("sounds/batterylow.wav")
        elif piste == 'PathAdjust':
            self.read = mixer.music.load("sounds/adjust1.wav")
        elif piste == 'TiredSearch':
            self.read = mixer.music.load("sounds/Oohmyhead.wav")
        elif piste == 'Dynamic':
            self.read = mixer.music.load("sounds/superstarsremix.wav")

        mixer.music.play(0)
        mixer.music.set_volume(self.vol)

    def temps(self):
        print(mixer.music.get_pos())









        #mixer.music.stop()

