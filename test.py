from squid import *
import time

rgb = Squid(13, 19, 26)
rgb.set_color(RED)
time.sleep(3)
rgb.set_color(GREEN)
time.sleep(3)
rgb.set_color(BLUE)
time.sleep(3)
rgb.set_color(WHITE)
time.sleep(3)
rgb.set_color(WHITE, 300)
time.sleep(3)
