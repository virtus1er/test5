# from matplotlib import pyplot as plt
import cv2
import  os

class cv():


	def	captCv(self):

		os.system('raspistill -o images/images.jpg -n -sh -100 -q 100 ')

		self.img =cv2.imread('images/images.jpg')
		self.imggris = cv2.cvtColor(self.img,cv2.COLOR_BGR2GRAY)
		self.imgrgb = cv2.cvtColor(self.img,cv2.COLOR_BGR2RGB)

		self.FiltreStop = cv2.CascadeClassifier("/home/pi/projet_wibi/images/data/cascade.xml")
		self.stop = self.FiltreStop.detectMultiScale(self.imggris,scaleFactor=1.2,minNeighbors=4,minSize=(200,200),maxSize=(500,500))

		self.n =len(self.stop)
		if self.n != 0 :
			for (x,y,w,h) in self.stop:
				cv2.rectangle(self.imgrgb,(x,y),(x+h,y+w),(0,255,0),5)


		cv2.imwrite("images/wibi.jpg",self.imgrgb)

		return self.n


